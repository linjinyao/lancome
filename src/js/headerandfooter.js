define(["jquery","template","url","cookie"],function($,template,Url){
  function HeaderAndFooter () {
    this.init();
    var log=$.cookie||[];
  }
  HeaderAndFooter.prototype = {
    constructor:HeaderAndFooter,
    init() {
      this.loadHeader();
      this.loadFooter();
      this.add();
    },
    loadHeader(){
      $("header").load("/html/include/header.html",() => {
        this.loguser();
        //渲染购物袋
        (function Cart() {
          $.cookie.json = true
          window.cart = [];
          const cart = window.cart = $.cookie("cart") || [];
          const data= {cart};
          const html = template("cart-template",data);
          $(".outer").html(html)
          
        })()
        //登录框
        $(".login").click(function(){
          $(".zhe").css({display:"block"});
          $(".log_box").css({display:"block"});
          // function disabledMouseWheel() {
          //   if (document.addEventListener) {
          //     document.addEventListener('DOMMouseScroll', scrollFunc, false);
          //   }//W3C
          //   window.onmousewheel = document.onmousewheel = scrollFunc;//IE/Opera/Chrome
          // }
          // function scrollFunc(evt) {
          //   return false;
          // }
          // window.onload=disabledMouseWheel;
          // disabledMouseWheel();
          $(".log_box_head").click(function(){
            $(".zhe").css({display:"none"});
            $(".log_box").css({display:"none"});
          })
        })
        //注册框
        $(".right_3").click(function(){
          $(".log_box").css({display:"none"});
          $(".zhuce").css({display:"block"})
        })
        $(".t_log").click(function(){
          $(".log_box").css({display:"block"});
          $(".zhuce").css({display:"none"})
        })
        $(".z_cha").click(function(){
          $(".zhe").css({display:"none"});
          $(".zhuce").css({display:"none"})
        })
        //搜索框
        $(".ss").click(function(){
          this.value = " ";
        })
        // 动态获取下拉菜单的宽
        $("#hufu").mouseenter(function() {
          var winWidth = window.innerWidth;
          $("#hufu").css({width:"winWidth"})
        });
        $("#caizhuang").mouseenter(function() {
          var winWidth = window.innerWidth;
          $("#caizhuang").css({width:"winWidth"})
        });
        $("#xiangshui").mouseenter(function() {
          var winWidth = window.innerWidth;
          $("#xiangshui").css({width:"winWidth"})
        });
        //跳转
        var src = $("dt");
        src.click(function(){
          location.href = "/html/list.html"
        });
        $(".logo").click(function(){
          location.href = "/index.html"
        });
        //总价
        (function price() {
          let sum = 0;
          $(".price").each((index, element) => {
            sum += Number($(element).text())
          })
          Number($(".e_right").text(sum)).toFixed(2)
        })();
        //登录操作
        this.Login();
        //注册操作
        this.Register();
      })
    },
    
    Login(){
      let usernameInput = $("#username"),
          passwordInput = $("#password"),
          loginBtn = $("#loginBtn");
      
      //绑定登录事件
      loginBtn.on("click", ()=>{
      let username = usernameInput.val(),
          password = passwordInput.val();
          console.log(username)
          console.log(password)
          //发送ajax请求，登录
          $.ajax({
            url: Url.baseUrlPhp + "/api/v1/login.php",
            method: "post",
            data: { username, password },
            dataType:"json",
            success: function(data){
             if(data.code === 1){
               location.reload();
               const log = [];
               log.push(usernameInput.val())
               $.cookie("log",log,{expores:7,path:"/"})
             }
            }
          })
          return false;
      })
    },
    Register(){
      this.usernameInput = $("#r_username");
      this.passwordInput = $("#r_password");
      //确认密码
      this.registerBtn = $(".c_btn");
      this.registerBtn.on("click", ()=>{

        let username = this.usernameInput.val(),
            password = this.passwordInput.val();
        
        //ajax
        $.ajax({
          url: Url.baseUrlPhp + "/api/v1/register.php",
          method: "post",
          data: {username, password},
          dataType: "json",
          success: function(data){
            if(data.res_body.status === 1){
              alert("注册成功")
            }else{
              alert("注册失败")
            }
          }
        })
         return false;
      })
    },
    loguser(){
      $.cookie.json = true;
       var log = $.cookie("log")||[];
      console.log(log)
      if(log.length>0)
      {
      $(".login").text(log)

      let html ="";
      html += `<a class="exit">[退出]</a>`;
      $(".login").append(html);
      $(".login").removeClass("login")
     
      }
      this.exit();
    },
    exit(){
    
      $(".exit").on("click",function(){
        $.cookie.json = true;
       
        // $.cookie("log",[],{expores:7,path:"/"})
        // const exit = $.cookie("log")
        // if(exit.length == 0){
          $.cookie("log",[])
          $("#l_exit").html(' ');
          let html ="";
          html += `登录/注册`;
          $("#l_exit").text(html);
          $("#l_exit").addClass("login")
          console.log(111)
          location.reload();
        // }
        
      })
    },
          
      
      
      

       
    loadFooter(){
      $("footer").load("/html/include/footer.html",function(){
        //搜索框
        $(".search_bb").click(function(){
          this.value = " ";
        })
      })
    },
    add(){
    },
    addListener(){
      $(".search :text").on("keyup",this.suggestHandler)
    },
    
    suggestHandler(event){
      console.log(111)
      const v = $(event.target).val();
    },
  }

  return new HeaderAndFooter();
});