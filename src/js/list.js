require(["./config"],function(){  
  require(["jquery","template","loadHF","cookie"],function($,template){
    function List(){
      this.loadList();
      this.addListener();    
    }

    List.prototype = {
      constructor:List,
      loadList(){
        $.getJSON("http://148.70.25.70:8080/app/mock/20/api/hots",res => {
          const data = {list:res.res_body.list}
          const html = template("list-template",data);
          $(".list_body").append(html)
        })
      },
      addListener() {
        console.log($("div.list_body"))
        $("div.list_body").on("click",".add-to-cart",this.addToCartHandler)
      },
      addToCartHandler(event) {
        const $parent = $(event.target).parent()
        const currProd = {
          id: $parent.data("id"),
          title:$parent.find(".prod-title").text(),
          img:$parent.find(".prod-img").attr("src"),
          price:$parent.find(".prod-price").text(),
          amount: 1
        }
        $.cookie.json = true;
        const cart = $.cookie("cart") || [];
        const has = cart.some(curr => {
          if(curr.id == currProd.id){
            curr.amount++;
            return true
          }
          return false
        });
        if(!has){
          cart.push(currProd)
        }
        $.cookie("cart",cart,{expores:7,path:"/"})
      }
    }

    new List();
  })
});