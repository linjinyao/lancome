require(["./config"],function(){
  require(["loadHF"],function(){
    require(["tools"],function(){
      //轮播
      var imgs = $(".lun"),
      len = imgs.length,
      currentIndex = 1,
      nextIndex = 2,
      duration = 5000,
      imgWidth = imgs[0].offsetWidth,
      ul = $(".imgs")[0],
      circles = $(".i"),
      isRunning = false;

      function move() {
      isRunning = true;
      var _left = -1 * nextIndex * imgWidth ;
      animate(ul, {left: _left}, 400, function() {
      if (nextIndex >= len) {
        currentIndex = 1;
        nextIndex = 2;
        ul.style.left = -1 * imgWidth + "px";
      }
      isRunning = false;
      });
      var circleIndex = nextIndex - 1;
      if (circleIndex > len - 3)
          circleIndex = 0;
          for (let i = 0; i < len - 2; i++) {
            circles[i].className = "i";
          }
          circles[circleIndex].className = "current i";
        currentIndex = nextIndex;
        ++nextIndex;

      }
      var timer = setInterval(move, duration);
      on($("#container"), "mouseenter", function() {
      clearInterval(timer);
      });
      on($("#container"), "mouseleave", function() {
      timer = setInterval(move, duration);
      });
      for(let i = 0; i < len - 2; i++) {
      on(circles[i], "click", function() {
      if (currentIndex === i + 1)
        return;
      nextIndex = i + 1;
      move();
      });
      }
    })
    require(["jquery","template"],function($,template){
      $.getJSON("http://localhost:80/api/v1/index.php",(res) => {
          const data = {list: res};
          console.log(res)
          console.log(data)
          const html = template("new-template",data);
          $(".list_foot").append(html)
        })
    })
  })
});