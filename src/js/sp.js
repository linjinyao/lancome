require(["./config"],function(){
  require(["loadHF","tools"],function(){
    //轮播
    on($(".centerlist_center_list")[0],"click",function(e){
      var src = event.target || event.srcElement;
      // if(src.className === "imgs_1"){
      //   $(".imgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489470707845782_460X460.jpg?version=20190211182022268"
      //   $(".bigimgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489470707845782_460X460.jpg?version=20190211182022268"
      // }
      if(src.className === "imgs_2"){
        $(".imgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489366811405009_460X460.jpg?version=20190211182022268"
        $(".bigimgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489366811405009_460X460.jpg?version=20190211182022268"
      }
      if(src.className === "imgs_3"){
        $(".imgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489182277168337_460X460.jpg?version=20190211182022268"
        $(".bigimgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489182277168337_460X460.jpg?version=20190211182022268"
      }
      if(src.className === "imgs_4"){
        $(".imgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489489881328645_460X460.jpg?version=20190211182022268"
        $(".bigimgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489489881328645_460X460.jpg?version=20190211182022268"
      }
      if(src.className === "imgs_5"){
        $(".imgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489489901889440_460X460.jpg?version=20190211182022268"
        $(".bigimgss")[0].src = "https://res.lancome.com.cn/resources/2019/1/31/15489489901889440_460X460.jpg?version=20190211182022268"
      }
    })
    //放大镜
    var
          middle = $(".imgs")[0],
          len = $(".len")[0],
          big = $(".bigimgs")[0],
          bigImg = $(".bigimgss")[0];
        on(middle, "mouseenter", function() {
          len.style.display = "block";
          big.style.display = "block";
        });
        on(middle, "mouseleave", function() {
          len.style.display = "none";
          big.style.display = "none";
        });
        on(middle, "mousemove", function(event) {
          var
            x = event.pageX - (middle.offsetLeft + middle.parentNode.offsetLeft),
            y = event.pageY - (middle.offsetTop + middle.parentNode.offsetTop);
            x = x - 115;
            y = y - 115;
            if (x < 0)
              x = 0;
            else if (x > 230)
              x = 230;
            if (y < 0)
              y = 0;
            else if (y > 230)
              y = 230;
            len.style.left = x + "px";
            len.style.top = y + "px";
            bigImg.style.left = -2 * x + "px";
            bigImg.style.top = -2 * y + "px";
          });
    //点击添加下划线
    on($(".color")[0],"click",function(e){
      var src = e.target;
      const aa = $(".aa");
      for(var i = 0;i < aa.length;i++) {
        aa[i].className = "aa"
      };
      if(src.parentNode.className == "aa"){
        src.parentNode.className = "aa bb"
      };
      
      
    })
    // $("#color").className = "color";
    //左右移动
    var prev = $(".prev")[0],
        next = $(".next")[0],
        aa = $(".aa")[0];
    on(next,"click",function(){
      var color = $(".color")[0];
      var _left = aa.offsetWidth;
      animate(color,{left: -_left},600,function(){
        _left = aa.offsetWidth + "px";
      })  
    });
    on(prev,"click",function(){
      var color = $(".color")[0];
      var _left = aa.offsetWidth;
      animate(color,{left: 0},800,function(){})
    });
    //下拉菜单
    $(".centerlist_center_xiangqing_5")[0].onclick = function(event) {
      event = event || window.event;
      event.stopPropagation 
        ? event.stopPropagation() 
        : event.cancelBubble = true;
      $(".menu")[0].style.display = "block";
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    $(".menu")[0].onclick = function(event) {
      event = event || window.event;
      var src = event.target || event.srcElement;
      $(".centerlist_center_xiangqing_5")[0].innerHTML = src.innerHTML;
    }
    document.onclick = function() {
      $(".menu")[0].style.display = "none";
    }
  })



  require(["jquery","url","loadHF"],function($,Url){
    const src = location.search;
    var id = src.slice(4);
    console.log(id);
    $.post({
      url: Url.baseUrlPhp + "/api/v1/detail.php",
      method: "post",
      data: { id: id },
      dataType:"json",
      success: function(data){
        
        console.log(data)
        const imgs = data[0].img;
        console.log(imgs)
        $(".imgs_1").attr("src",imgs)
        $(".imgss").attr("src",imgs)
        $(".bigimgss").attr("src",imgs)
      }
    })
      
    })
    
    // $.getJSON("http://localhost:80/api/v1/detail.php",(res) => {
    //       const data = {list: res};
    //       console.log(res)
    //       // const html = template("new-template",data);
    //       // $(".list_foot").append(html)
    //     })
  

});